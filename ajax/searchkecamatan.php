<?php

require '../functions.php';


// perhalaman ada berapa row
$dataPerPagination = 5;

// menangkap keyword dari ajax 
$keyword = $_GET["keyword"];

// $jumlahTotalIndex = count(query("SELECT * FROM kecamatan"));
$jumlahTotalIndexSearch = count(query("SELECT * FROM kecamatan WHERE kecamatanNAMA LIKE '%$keyword%'"));
$jumlahTotalIndex = $jumlahTotalIndexSearch; //timpa

$jumlahHalamanSearch = ceil($jumlahTotalIndex / $dataPerPagination);
$jumlahHalaman = $jumlahHalamanSearch;

$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;




// starting point dari data yang di search
$awalDataSearch = ($dataPerPagination * $activePage) - $dataPerPagination;
$awalData = $awalDataSearch; //timpa

// query akhir
$selectkecamatan = cari($keyword, $awalDataSearch, $dataPerPagination);
?>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">No</th>
            <th scope="col">Kode Kecamatan</th>
            <th scope="col">Nama Kecamatan</th>
            <th scope="col" style="width:10%">Alamat Kecamatan</th>
            <th scope="col" style="width:25%">Keterangan Kecamatan</th>
            <th scope="col">Tanggal Kecamatan</th>
            <th scope="col">Foto Kecamatan</th>
            <th scope="col">Kode Kabupaten</th>
            <th scope="col">Action</th>
        </tr>
    </thead>

    <tbody>

        <?php $x = $awalData + 1; ?>
        <?php foreach ($selectkecamatan as $kecamatan) : ?>
            <tr>

                <td><?= $x; ?></td>
                <td><?= $kecamatan["kecamatanKODE"]; ?></td>
                <td><?= $kecamatan["kecamatanNAMA"]; ?></td>
                <td><?= $kecamatan["kecamatanALAMAT"]; ?></td>
                <td><?= $kecamatan["kecamatanKET"]; ?></td>
                <td><?= $kecamatan["kecamatanTGL"]; ?></td>

                <td><img src="../img/<?= $kecamatan["kecamatanFOTO"]; ?>" alt="" width="50" height="50"> </td>
                <td><?= $kecamatan["kabupatenKODE"]; ?></td>


                <td>
                    <a href="editkecamatan.php?kecamatanKODE=<?= $kecamatan["kecamatanKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a>
                    <a href="hapuskecamatan.php?kecamatanKODE=<?= $kecamatan["kecamatanKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>
                </td>



            </tr>
            <?php $x++; ?>
        <?php endforeach; ?>
    </tbody>
</table>

<!-- pagination -->
<nav aria-label="Page navigation example">
    <ul class="pagination">

        <?php if ($activePage > 1) : ?>
            <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
        <?php endif; ?>

        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

            <?php if ($i == $activePage) : ?>

                <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
            <?php else : ?>
                <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


            <?php endif; ?>
        <?php endfor; ?>

        <?php if ($activePage < $jumlahHalaman) : ?>
            <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
        <?php endif; ?>
    </ul>
</nav>