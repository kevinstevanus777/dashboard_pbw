<?php
session_start();

if (!isset($_SESSION["login"])) {

    header("Location: login.php");
}

include '../_header.php';

require '../functions.php';

$selectkategoriberita = query("SELECT * FROM kategoriberita ");


if (isset($_POST["submit"])) {
    if (insertkategoriberita($_POST) > 0) {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    } else {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    }
}


?>
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard Sistem Pesona Jawa</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
                    <li><i class="fa fa-laptop"></i>Dashboard</li>
                </ol>
            </div>
        </div>
    </section>



    <form class="form-horizontal" action="" method="post">




        <div class="form-group">
            <label for="KodeKategoriBerita" class="col-sm-2 control-label">Kode Kategori Berita</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="KodeKategoriBerita" name="KodeKategoriBerita" aria-describedby="emailHelp" placeholder="Kode Kategori Berita">
            </div>
        </div>

        <div class="form-group">
            <label for="NamaKategoriBerita" class="col-sm-2 control-label">Kode Kategori Berita</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="NamaKategoriBerita" name="NamaKategoriBerita" aria-describedby="emailHelp" placeholder="Nama Kategori Berita">
            </div>
        </div>


        <div class="form-group">
            <label for="KeteranganKategoriBerita" class="col-sm-2 control-label">Keterangan Kategori Berita</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="KeteranganKategoriBerita" name="KeteranganKategoriBerita" aria-describedby="emailHelp" placeholder="Keterangan Kategori Berita">
            </div>
        </div>





        <div class="form-group">

            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>

            </div>
        </div>




    </form>




    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode Kategori</th>
                <th scope="col">Nama Kategori</th>
                <th scope="col">Keterangan Kategori</th>
                <th scope="col">Action</th>
            </tr>
        </thead>

        <tbody>

            <?php $x = 1; ?>
            <?php foreach ($selectkategoriberita as $data) : ?>
                <tr>

                    <td><?= $x; ?></td>
                    <td><?= $data["kategoriberitaKODE"]; ?></td>
                    <td><?= $data["kategoriberitaNAMA"]; ?></td>
                    <td><?= $data["kategoriberitaKET"]; ?></td>
                    <td><button type="" class="btn btn-secondary">A Button</button></td>

                </tr>
                <?php $x++; ?>
            <?php endforeach; ?>

        </tbody>
    </table>


</section>



<?php include '../_footer.php'; ?>