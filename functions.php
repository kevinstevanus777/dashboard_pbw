<?php

$connection = mysqli_connect('localhost', 'root', '', 'dbpesona');


function query($query)
{
    global $connection;

    $result = mysqli_query($connection, $query);

    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }

    return $rows;
}


function cari($keyword, $awalDataSearch, $dataPerPagination)
{
    $query = "SELECT * FROM kecamatan WHERE kecamatanNAMA LIKE '%$keyword%' LIMIT $awalDataSearch, $dataPerPagination ";
    return query($query);
}

function ubahkecamatan($data)
{
    global $connection;

    $kecamatankode = htmlspecialchars($data["KodeKecamatan"]);
    $kecamatannama = htmlspecialchars($data["NamaKecamatan"]);
    $kecamatanalamat = htmlspecialchars($data["AlamatKecamatan"]);
    $kecamatanket = htmlspecialchars($data["KeteranganKecamatan"]);
    $kecamatantgl = htmlspecialchars($data["TanggalKecamatan"]);
    // $kecamatanfoto = htmlspecialchars($data[""]);
    $kabupatenkode = htmlspecialchars($data["NamaKabupaten"]);
    $gambarlama = htmlspecialchars($data["gambarlama"]);

    if ($_FILES['GambarKecamatan']['error'] === 4) {
        $gambar = $gambarlama;
    } else {
        $gambar = upload();
    }

    // $gambar = htmlspecialchars($data["GambarKecamatan"]);


    $query = "UPDATE kecamatan SET
    kecamatanKODE = '$kecamatankode',
    kecamatanNAMA = '$kecamatannama',
    kecamatanALAMAT = '$kecamatanalamat',
    kecamatanKET = '$kecamatanket',
    kecamatanTGL = '$kecamatantgl',
    kecamatanFOTO = '$gambar',
    kabupatenKODE = '$kabupatenkode'
    WHERE kecamatanKODE = '$kecamatankode'
    ";



    return query($query);
}




function insertkecamatan($data)
{
    global $connection;

    // ambil data
    $kodekecamatan = htmlspecialchars($data["KodeKecamatan"]);
    $namakecamatan = htmlspecialchars($data["NamaKecamatan"]);
    $alamatkecamatan = htmlspecialchars($data["AlamatKecamatan"]);
    $keterangankecamatan = htmlspecialchars($data["KeteranganKecamatan"]);
    $tanggalkecamatan = htmlspecialchars($data["TanggalKecamatan"]);
    $namakabupaten = htmlspecialchars($data["NamaKabupaten"]);



    // $gambarkecamatan = htmlspecialchars($data["UploadGambar"]);


    // upload gambar
    $gambar =  upload();
    if ($gambar === false) {
        return false;
    }




    $query = "INSERT INTO kecamatan
    VALUES('$kodekecamatan','$namakecamatan','$alamatkecamatan','$keterangankecamatan','$tanggalkecamatan','$gambar','$namakabupaten')";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}


function upload()
{

    $namafile = $_FILES['GambarKecamatan']['name'];
    $ukuranfile = $_FILES['GambarKecamatan']['size'];
    $error = $_FILES['GambarKecamatan']['error'];
    $tmpname = $_FILES['GambarKecamatan']['tmp_name'];


    if ($error === 4) {
        echo "
        <script>
            alert('pilih gambar dulu');
        </script>
        
        ";
        return false;
    }



    // imageonly checker
    $imageextension = ['jpg', 'jpeg', 'png'];
    // delimiter
    $ekstensigambar = explode('.', $namafile);
    // force small character dan ambil ekstensi aja
    $ekstensigambar = strtolower(end($ekstensigambar));




    if (!in_array($ekstensigambar, $imageextension)) {
        echo "<script>
        alert('yang di upload bukan gambar');
        </script>";
        return false;
    }


    // still not working
    if ($ukuranfile > 3000000) {

        echo "<script>
        alert('cek ukuran file');
        </script>";
        return false;
    }

    $namafilebaru = uniqid();
    $namafilebaru .= '.';
    $namafilebaru .= $ekstensigambar;
    // var_dump($namafilebaru);

    // pindah gambar 
    move_uploaded_file($tmpname, '../img/' . $namafilebaru);
    return $namafilebaru;
}


function insertkabupaten($data)
{

    global $connection;

    // ambil data
    $kodekabupaten = htmlspecialchars($data["KodeKabupaten"]);
    $namakabupaten = htmlspecialchars($data["NamaKabupaten"]);
    $alamatkabupaten = htmlspecialchars($data["AlamatKabupaten"]);
    $keterangankabupaten = htmlspecialchars($data["KeteranganKabupaten"]);
    $query = "INSERT INTO kabupaten
    VALUES('$kodekabupaten','$namakabupaten','$alamatkabupaten','$keterangankabupaten','','')";

    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}


function insertkategoriberita($data)
{
    global $connection;


    // ambil data
    $kodekategori = htmlspecialchars($data["KodeKategoriBerita"]);
    $namakategori = htmlspecialchars($data["NamaKategoriBerita"]);
    $keterangankategori = htmlspecialchars($data["KeteranganKategoriBerita"]);

    $query = "INSERT INTO kategoriberita
    VALUES('$kodekategori','$namakategori','$keterangankategori')";

    // mysqli_query($connection, $query);
    // return mysqli_affected_rows($connection);
    return (query($query));
}



function insertkategoriwisata($data)
{

    global $connection;

    $kodekategoriwisata = htmlspecialchars($data["KategoriKodeWisata"]);
    $namakategori = htmlspecialchars($data["NamaKategori"]);
    $kategoriketerangan = htmlspecialchars($data["KategoriKeterangan"]);
    $referensikategori = htmlspecialchars($data["ReferensiKategori"]);

    $query  = "INSERT INTO kategoriwisata VALUES('$kodekategoriwisata','$namakategori','$kategoriketerangan','$referensikategori')";


    mysqli_query($connection, $query);
    return mysqli_affected_rows($connection);
}


function ubahkategoriwisata($data)
{
    global $connection;
    $kodekategoriwisata = htmlspecialchars($data["KategoriKodeWisata"]);
    $namakategori = htmlspecialchars($data["NamaKategori"]);
    $kategoriketerangan = htmlspecialchars($data["KategoriKeterangan"]);
    $referensikategori = htmlspecialchars($data["ReferensiKategori"]);


    $query = "UPDATE kategoriwisata SET
    kategoriKODE = '$kodekategoriwisata',
    kategoriNAMA = '$namakategori',
    kategoriKET = '$kategoriketerangan',
    kategoriREFERENCE = '$referensikategori'
    WHERE kategoriKODE = '$kodekategoriwisata'
    ";

    return query($query);
}




function hapuskategoriwisata($id)
{


    global $connection;

    mysqli_query($connection, "DELETE FROM kategoriwisata WHERE kategoriKODE = '$id'");
    return mysqli_affected_rows($connection);
}


function registrasi($data)
{

    global $connection;

    $username = strtolower(stripslashes($data["username"]));
    $password = mysqli_real_escape_string($connection, $data["password"]);
    $password2 = mysqli_real_escape_string($connection, $data["password2"]);


    // cek username udah ada atau belom
    $result = mysqli_query($connection, "SELECT USERNAME FROM USERS WHERE username = '$username'");
    if (mysqli_fetch_assoc($result)) {
        echo "
    <script>
    alert('username exists')
    </script>
    ";
        return false;
    }

    // cek konfirmasi password
    if ($password != $password2) {
        echo "
        <script>
        alert('password berbeda');
        </script>
        ";
        return false;
    } else {
        echo mysqli_error($connection);
    }

    // enkripsi password

    $password = password_hash($password, PASSWORD_DEFAULT);

    // tambah user ke database
    mysqli_query($connection, "INSERT INTO USERS VALUES(
        '','$username','$password'
    )");

    return mysqli_affected_rows($connection);
}
