$(function () {
    $("#datepicker").datepicker({
        numberOfMonths: [1, 2],
        dateFormat: 'yy-mm-dd',
        changeYear: true
    });
});