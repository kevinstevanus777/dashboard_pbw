/*
// ambil elemen2 
var keyword = document.getElementById('keyword');
var tombolCari = document.getElementById('tombol-cari');
var tableContainer = document.getElementById('tablecontainer');

keyword.addEventListener('keyup', function () {



    // buat object ajax
    var xhr = new XMLHttpRequest();


    // cek kesiapan ajax
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            // console.log(xhr.responseText);

            // tableContainer.innerHTML = xhr.responseText;
            // console.log(xhr.responseText);
            tableContainer.innerHTML = xhr.responseText;
            // tableContainer.innerHTML = xhr.responseText;
        }
    }

    // eksekusi ajax
    xhr.open('GET', '../ajax/searchkecamatan.php?keyword=' + keyword.value, true);
    // xhr.open('GET', '../ajax/test.txt', true);
    xhr.send();

});
*/





$(document).ready(function () {

    // hilangkan tombol cari
    $('#tombol-cari').hide();


    // event ketika keyword ditulis
    $('#keyword').on('keyup', function () {

        // show loading icon
        $('.loader').show();

        // ajax menggunakan load
        // $('#tablecontainer').load('../ajax/searchkecamatan.php?keyword=' + $('#keyword').val());


        $.get('../ajax/searchkecamatan.php?keyword=' + $('#keyword').val(), function (data) {

            $('#tablecontainer').html(data);

            $('.loader').hide();
        });

    });
});