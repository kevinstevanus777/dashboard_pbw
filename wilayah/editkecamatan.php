<?php
session_start();

if (!isset($_SESSION["login"])) {

    header("Location: login.php");
}





include '../_header.php';
require '../functions.php';

$id = $_GET["kecamatanKODE"];

$selectkecamatan = query("SELECT * FROM kecamatan WHERE kecamatanKODE = '$id'")[0];
$selectkabupaten = query("SELECT * FROM kabupaten");



if (isset($_POST["submit"])) {
    if (ubahkecamatan($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = 'kecamatan.php'
</script>";
    } else {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = 'kecamatan.php'
</script>";
    }
}

?>


<!-- begin of datepicker -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $(function() {
        $("#datepicker").datepicker({
            numberOfMonths: [1, 2],
            dateFormat: 'yy-mm-dd',
            changeYear: true
        });
    });
</script>
<!-- end of datepicker -->

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard Sistem Pesona Jawa</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="../index.php">Home</a></li>
                    <li><i class="fa fa-laptop"></i>Dashboard</li>
                </ol>
            </div>
        </div>
    </section>




    <form action="" class="form-horizontal" method="post" enctype="multipart/form-data">

        <input type="hidden" name="gambarlama" value="<?= $selectkecamatan["kecamatanFOTO"]; ?>">



        <div class='form-group'>
            <label for='KodeKecamatan' class='col-sm-2 control-label'>KodeKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='KodeKecamatan' name='KodeKecamatan' aria-describedby='emailHelp' placeholder='KodeKecamatan' value="<?= $selectkecamatan["kecamatanKODE"]; ?>">
            </div>
        </div>


        <div class='form-group'>
            <label for='NamaKecamatan' class='col-sm-2 control-label'>NamaKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='NamaKecamatan' name='NamaKecamatan' aria-describedby='emailHelp' placeholder='NamaKecamatan' value="<?= $selectkecamatan["kecamatanNAMA"] ?>">
            </div>
        </div>


        <div class='form-group'>
            <label for='AlamatKecamatan' class='col-sm-2 control-label'>AlamatKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='AlamatKecamatan' name='AlamatKecamatan' aria-describedby='emailHelp' placeholder='AlamatKecamatan' value="<?= $selectkecamatan["kecamatanALAMAT"] ?>">
            </div>
        </div>


        <div class='form-group'>
            <label for='KeteranganKecamatan' class='col-sm-2 control-label'>KeteranganKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='KeteranganKecamatan' name='KeteranganKecamatan' aria-describedby='emailHelp' placeholder='KeteranganKecamatan' value="<?= $selectkecamatan["kecamatanKET"] ?>">
            </div>
        </div>

        <div class='form-group'>
            <label for='datepicker' class='col-sm-2 control-label'>TanggalKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='datepicker' name='TanggalKecamatan' aria-describedby='emailHelp' placeholder='TanggalKecamatan' value="<?= $selectkecamatan["kecamatanTGL"] ?>">
                <script></script>
            </div>
        </div>

        <div class='form-group form-group-lg'>
            <label for='NamaKabupaten' class='col-sm-2 control-label'>NamaKabupaten</label>
            <div class='col-sm-4'>

                <select name="NamaKabupaten" id="NamaKabupaten" class="form-control">

                    <?php foreach ($selectkabupaten as $kabupaten) : ?>

                        <option value="<?php echo $kabupaten["kabupatenKODE"]; ?>" <?php
                                                                                        if ($kabupaten["kabupatenKODE"] === $selectkecamatan["kabupatenKODE"]) {
                                                                                            echo "selected";
                                                                                        } ?>>
                            <?php echo $kabupaten["kabupatenNAMA"]; ?>
                        </option>
                    <?php endforeach; ?>
                </select>


                <!-- <input type='text' class='form-control' id='NamaKabupaten' name='NamaKabupaten' aria-describedby='emailHelp' placeholder='NamaKabupaten'> -->
            </div>
        </div>


        <div class='form-group'>
            <label for='GambarKecamatan' class='col-sm-2 control-label'>Upload Gambar</label>

            <img src="../img/<?= $selectkecamatan['kecamatanFOTO']; ?>" alt="" width="50" height="50">
            <div class='col-sm-4'>
                <input type='file' id='GambarKecamatan' name='GambarKecamatan'>
            </div>
        </div>


        <!-- punya orang -->
        <!-- <div class="form-group form-group-lg">
            <label class="col-sm-3 control-label" for="kecamatanfoto">Unggah Gambar</label>
            <div class="col-sm-6">
                <input type="file" id="kecamatanfoto" name="gambar">
            </div>
        </div> -->


        <div class='form-group'>
            <div class='col-sm-offset-2 col-sm-10'>
                <button type='submit' name='submit' class='btn btn-primary'>Submit</button>
            </div>
        </div>

    </form>



    <?php include '../_footer.php'; ?>