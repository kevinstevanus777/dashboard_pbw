<?php

session_start();

if (!isset($_SESSION["login"])) {

    header("Location: ../login.php");
}




include '../_header.php';
require '../functions.php';

$selectkabupaten = query("SELECT * FROM kabupaten");

if (isset($_POST["submit"])) {



    if (insertkabupaten($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}

?>





<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard Sistem Pesona Jawa</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
                    <li><i class="fa fa-laptop"></i>Dashboard</li>
                </ol>
            </div>
        </div>
    </section>



    <form class="form-horizontal" action="" method="post">




        <!-- untuk kode kabupaten -->
        <div class="form-group">
            <label for="KodeKabupaten" class="col-sm-2 control-label">Kode Kabupaten</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="KodeKabupaten" name="KodeKabupaten" aria-describedby="emailHelp" placeholder="Kode Kabupaten">
            </div>
        </div>

        <!-- untuk nama kabupaten -->
        <div class="form-group">
            <label for="NamaKabupaten" class="col-sm-2 control-label">Nama Kabupaten</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="NamaKabupaten" name="NamaKabupaten" aria-describedby="emailHelp" placeholder="NamaKabupaten">
            </div>
        </div>


        <!-- untuk alamat kabupaten -->
        <div class="form-group">
            <label for="AlamatKabupaten" class="col-sm-2 control-label">Alamat Kabupaten</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="AlamatKabupaten" name="AlamatKabupaten" aria-describedby="emailHelp" placeholder="Alamat Kabupaten">
            </div>
        </div>

        <div class="form-group">
            <label for="KeteranganKabupaten" class="col-sm-2 control-label">Keterangan Kabupaten</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="KeteranganKabupaten" name="KeteranganKabupaten" aria-describedby="emailHelp" placeholder="Keterangan Kabupaten">
            </div>
        </div>




        <div class="form-group">

            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>

            </div>
        </div>




    </form>




    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode Kabupaten</th>
                <th scope="col">Nama Kabupaten</th>
                <th scope="col">Alamat Kabupaten</th>
                <th scope="col">Keterangan Kabupaten</th>
                <th scope="col">Foto Kabupaten</th>
                <th scope="col">Foto Icon Ket Kabupaten</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>

        <tbody>
            <?php $i = 1; ?>
            <?php foreach ($selectkabupaten as $kabupaten) : ?>
                <tr>

                    <td><?= $i; ?></td>
                    <td><?= $kabupaten["kabupatenKODE"]; ?></td>
                    <td><?= $kabupaten["kabupatenNAMA"]; ?></td>
                    <td><?= $kabupaten["kabupatenALAMAT"]; ?></td>

                    <td><?= $kabupaten["kabupatenKET"]; ?></td>
                    <td><?= $kabupaten["kabupatenFOTOICON"]; ?></td>
                    <td><?= $kabupaten["kabupatenFOTOICONKET"]; ?></td>
                    <td> <button type="submit" class="btn btn-primary">A Button</button></td>

                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>

        </tbody>
    </table>


</section>







<?php include '../_footer.php'; ?>