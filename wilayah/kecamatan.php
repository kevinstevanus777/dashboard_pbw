<?php

session_start();

if (!isset($_SESSION["login"])) {

    header("Location: ../login.php");
}





include '../_header.php';

require '../functions.php';





// pagination

$dataPerPagination = 5;
// $querytotaldata = mysqli_query($connection, "SELECT * FROM kecamatan");
$jumlahTotalIndex = count(query("SELECT * FROM kecamatan"));
$jumlahHalaman = ceil($jumlahTotalIndex / $dataPerPagination);


$activePage = (isset($_GET["page"])) ? $_GET["page"] : 1;

$awalData = ($dataPerPagination * $activePage) - $dataPerPagination;

// original limit
$selectkecamatan = query("SELECT * FROM kecamatan LIMIT $awalData , $dataPerPagination");

// yang ini bisa (utk pake LIKE)
// $selectkecamatan = query("SELECT * FROM kecamatan where kecamatanNAMA LIKE '%anew%'");

// agak bisa tapi dikit
// $selectkecamatan = query("SELECT * FROM kecamatan where kecamatanNAMA LIKE '%Kecamatan%' LIMIT 0,5");


if (isset($_POST["search"])) {


    // $jumlahTotalIndexSearch = count(cari($_POST["keyword"]));
    $keyword = $_POST["keyword"];
    $jumlahTotalIndexSearch = count(query("SELECT * FROM kecamatan WHERE kecamatanNAMA LIKE '%$keyword%'"));

    $jumlahHalamanSearch = ceil($jumlahTotalIndex / $dataPerPagination);


    $awalDataSearch = ($dataPerPagination * $activePage) - $dataPerPagination;

    // is this true?
    $jumlahTotalIndex = $jumlahTotalIndexSearch;

    $selectkecamatan = cari($_POST["keyword"], $awalDataSearch, $dataPerPagination);

    // $searchstring = $_POST["search"];
    // var_dump($searchstring);
    // die;
}


// $selectkecamatan = query("SELECT * FROM kecamatan");
$selectkabupaten = query("SELECT * FROM kabupaten");

if (isset($_POST["submit"])) {



    if (insertkecamatan($_POST) > 0) {
        echo "<script>
    alert('data berhasil ditambahkan!');
    document.location.href = ''
</script>";
    } else {
        echo "<script>
    alert('data tidak berhasil ditambahkan!');
    document.location.href = ''
</script>";
    }
}

?>

<style>
    .loader {
        width: 50px;
        position: absolute;
        top: 700;
        z-index: -1;
        left: 380;
        display: none;
    }
</style>



<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard Sistem Pesona Jawa</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="../index.php">Home</a></li>
                    <li><i class="fa fa-laptop"></i>Dashboard</li>
                </ol>
            </div>
        </div>
    </section>




    <form action="" class="form-horizontal" method="post" enctype="multipart/form-data">


        <div class='form-group'>
            <label for='KodeKecamatan' class='col-sm-2 control-label'>KodeKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='KodeKecamatan' name='KodeKecamatan' aria-describedby='emailHelp' placeholder='KodeKecamatan'>
            </div>
        </div>


        <div class='form-group'>
            <label for='NamaKecamatan' class='col-sm-2 control-label'>NamaKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='NamaKecamatan' name='NamaKecamatan' aria-describedby='emailHelp' placeholder='NamaKecamatan'>
            </div>
        </div>


        <div class='form-group'>
            <label for='AlamatKecamatan' class='col-sm-2 control-label'>AlamatKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='AlamatKecamatan' name='AlamatKecamatan' aria-describedby='emailHelp' placeholder='AlamatKecamatan'>
            </div>
        </div>


        <div class='form-group'>
            <label for='KeteranganKecamatan' class='col-sm-2 control-label'>KeteranganKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='KeteranganKecamatan' name='KeteranganKecamatan' aria-describedby='emailHelp' placeholder='KeteranganKecamatan'>
            </div>
        </div>

        <div class='form-group'>
            <label for='datepicker' class='col-sm-2 control-label'>TanggalKecamatan</label>
            <div class='col-sm-4'>
                <input type='text' class='form-control' id='datepicker' name='TanggalKecamatan' aria-describedby='emailHelp' placeholder='TanggalKecamatan'>
                <script></script>
            </div>
        </div>

        <div class='form-group form-group-lg'>
            <label for='NamaKabupaten' class='col-sm-2 control-label'>NamaKabupaten</label>
            <div class='col-sm-4'>

                <select name="NamaKabupaten" id="NamaKabupaten" class="form-control">

                    <?php foreach ($selectkabupaten as $kabupaten) : ?>

                        <option value="<?= $kabupaten["kabupatenKODE"]; ?>">
                            <?= $kabupaten["kabupatenNAMA"]; ?>
                        </option>
                    <?php endforeach; ?>
                </select>


                <!-- <input type='text' class='form-control' id='NamaKabupaten' name='NamaKabupaten' aria-describedby='emailHelp' placeholder='NamaKabupaten'> -->
            </div>
        </div>


        <div class='form-group'>
            <label for='GambarKecamatan' class='col-sm-2 control-label'>Upload Gambar</label>

            <div class='col-sm-4'>
                <input type='file' id='GambarKecamatan' name='GambarKecamatan'>
            </div>
        </div>


        <!-- punya orang -->
        <!-- <div class="form-group form-group-lg">
            <label class="col-sm-3 control-label" for="kecamatanfoto">Unggah Gambar</label>
            <div class="col-sm-6">
                <input type="file" id="kecamatanfoto" name="gambar">
            </div>
        </div> -->


        <div class='form-group'>
            <div class='col-sm-offset-2 col-sm-10'>
                <button type='submit' name='submit' class='btn btn-primary'>Submit</button>
            </div>
        </div>

    </form>


    <!-- searchbox -->
    <form class="navbar-form" method="post" action="">
        <input class="form-control" placeholder="Search" type="text" name="keyword" autofocus="" autocomplete="off" id="keyword">
        <button type='cari' name='search' class='btn btn-primary' id="tombol-cari">search</button>
        <img src="../img/loader.gif" class="loader">
    </form>








    <div id="tablecontainer">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Kode Kecamatan</th>
                    <th scope="col">Nama Kecamatan</th>
                    <th scope="col" style="width:10%">Alamat Kecamatan</th>
                    <th scope="col" style="width:25%">Keterangan Kecamatan</th>
                    <th scope="col">Tanggal Kecamatan</th>
                    <th scope="col">Foto Kecamatan</th>
                    <th scope="col">Kode Kabupaten</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>

            <tbody>

                <?php $x = $awalData + 1; ?>
                <?php foreach ($selectkecamatan as $kecamatan) : ?>
                    <tr>

                        <td><?= $x; ?></td>
                        <td><?= $kecamatan["kecamatanKODE"]; ?></td>
                        <td><?= $kecamatan["kecamatanNAMA"]; ?></td>
                        <td><?= $kecamatan["kecamatanALAMAT"]; ?></td>
                        <td><?= $kecamatan["kecamatanKET"]; ?></td>
                        <td><?= $kecamatan["kecamatanTGL"]; ?></td>

                        <td><img src="../img/<?= $kecamatan["kecamatanFOTO"]; ?>" alt="" width="50" height="50"> </td>
                        <td><?= $kecamatan["kabupatenKODE"]; ?></td>


                        <td>
                            <a href="editkecamatan.php?kecamatanKODE=<?= $kecamatan["kecamatanKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a>
                            <a href="hapuskecamatan.php?kecamatanKODE=<?= $kecamatan["kecamatanKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>
                        </td>



                    </tr>
                    <?php $x++; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
        <!-- pagination -->
        <nav aria-label="Page navigation example">
            <ul class="pagination">

                <?php if ($activePage > 1) : ?>
                    <li class="page-item"><a class="page-link" href="?page=<?= $activePage - 1; ?>">Previous</a></li>
                <?php endif; ?>

                <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>

                    <?php if ($i == $activePage) : ?>

                        <li class="page-item"><a class="page-link" style="color:red" href="?page=<?= $i ?>"><?= $i; ?></a></li>
                    <?php else : ?>
                        <li class="page-item"><a class="page-link" href="?page=<?= $i ?>"><?= $i; ?></a></li>


                    <?php endif; ?>
                <?php endfor; ?>

                <?php if ($activePage < $jumlahHalaman) : ?>
                    <li class="page-item"><a class="page-link" href="?page=<?= $activePage + 1; ?>">Next</a></li>
                <?php endif; ?>
            </ul>
        </nav>


    </div>




</section>

<!-- script boleh berada diatas kalau pakai jquery! -->
<script src="../js/livesearch.js"></script>

<?php

include '../_footer.php';
?>