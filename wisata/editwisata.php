<?php
include '../_header.php';
require '../functions.php';


$kategorikode = $_GET["kategoriKODE"];
$selectkategoriwisata = query("SELECT * FROM kategoriwisata WHERE kategoriKODE = '$kategorikode'")[0];


if (isset($_POST["submit"])) {
    if (ubahkategoriwisata($_POST) > 0) {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= 'kategoriwisata.php'
        </script>";
    } else {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= 'kategoriwisata.php'
        </script>";
    }
}



?>


<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard Sistem Pesona Jawa</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="index.php">Home</a></li>
                    <li><i class="fa fa-laptop"></i>Dashboard</li>
                </ol>
            </div>
        </div>
    </section>



    <form action="" class="form-horizontal" method="post">


        <div class="form-group">
            <label for="KategoriKodeWisata" class="col-sm-2 control-label">Kode Kategori Wisata</label>

            <div class="col-sm-4">

                <input type="text" value="<?= $selectkategoriwisata["kategoriKODE"]; ?>" class="form-control" id="KategoriKodeWisata" name="KategoriKodeWisata" aria-describedby="emailHelp" placeholder="Kode Kategori Wisata">
            </div>
        </div>

        <div class="form-group">
            <label for="NamaKategori" class="col-sm-2 control-label">Nama Kategori</label>

            <div class="col-sm-4">

                <input type="text" value="<?= $selectkategoriwisata["kategoriNAMA"]; ?>" class="form-control" id="NamaKategori" name="NamaKategori" aria-describedby="emailHelp" placeholder="Nama Kategori">
            </div>
        </div>

        <div class="form-group">
            <label for="KategoriKeterangan" class="col-sm-2 control-label">Keterangan Kategori</label>

            <div class="col-sm-4">

                <input type="text" value="<?= $selectkategoriwisata["kategoriKET"]; ?>" class="form-control" id="KategoriKeterangan" name="KategoriKeterangan" aria-describedby="emailHelp" placeholder="Keterangan Kategori">
            </div>
        </div>

        <div class="form-group">
            <label for="ReferensiKategori" class="col-sm-2 control-label">Referensi Kategori</label>

            <div class="col-sm-4">

                <input type="text" value="<?= $selectkategoriwisata["kategoriREFERENCE"]; ?>" class="form-control" id="ReferensiKategori" name="ReferensiKategori" aria-describedby="emailHelp" placeholder="Referensi Kategori">
            </div>
        </div>

        <div class="form-group">

            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>

            </div>
        </div>


    </form>


</section>




<?php include '../_footer.php'; ?>