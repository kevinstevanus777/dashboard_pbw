<?php
include '../_header.php';

require '../functions.php';

$selectkategoriwisata = query("SELECT * FROM kategoriwisata");


if (isset($_POST["submit"])) {
    if (insertkategoriwisata($_POST) > 0) {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    } else {
        echo "<script>
        alert('data berhasil ditambahkan!');
        document.location.href= ''
        </script>";
    }
}

?>

<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard Sistem Pesona Jawa</h3>
                <ol class="breadcrumb">
                    <li><i class="fa fa-home"></i><a href="../index.php">Home</a></li>
                    <li><i class="fa fa-laptop"></i>Dashboard</li>
                </ol>
            </div>
        </div>
    </section>

    <form action="" class="form-horizontal" method="post">


        <div class="form-group">
            <label for="KategoriKodeWisata" class="col-sm-2 control-label">Kode Kategori Wisata</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="KategoriKodeWisata" name="KategoriKodeWisata" aria-describedby="emailHelp" placeholder="Kode Kategori Wisata">
            </div>
        </div>

        <div class="form-group">
            <label for="NamaKategori" class="col-sm-2 control-label">Nama Kategori</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="NamaKategori" name="NamaKategori" aria-describedby="emailHelp" placeholder="Nama Kategori">
            </div>
        </div>

        <div class="form-group">
            <label for="KategoriKeterangan" class="col-sm-2 control-label">Keterangan Kategori</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="KategoriKeterangan" name="KategoriKeterangan" aria-describedby="emailHelp" placeholder="Keterangan Kategori">
            </div>
        </div>

        <div class="form-group">
            <label for="ReferensiKategori" class="col-sm-2 control-label">Referensi Kategori</label>

            <div class="col-sm-4">

                <input type="text" class="form-control" id="ReferensiKategori" name="ReferensiKategori" aria-describedby="emailHelp" placeholder="Referensi Kategori">
            </div>
        </div>

        <div class="form-group">

            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" name="submit" class="btn btn-primary">Submit</button>

            </div>
        </div>


    </form>




    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode Kategori</th>
                <th scope="col">Nama Kategori</th>
                <th scope="col">Keterangan Kategori</th>
                <th scope="col">Referensi Kategori</th>
                <th scope="col">Action</th>
            </tr>
        </thead>

        <tbody>

            <?php $x = 1; ?>
            <?php foreach ($selectkategoriwisata as $data) : ?>
                <tr>

                    <td><?= $x; ?></td>
                    <td><?= $data["kategoriKODE"]; ?></td>
                    <td><?= $data["kategoriNAMA"]; ?></td>
                    <td><?= $data["kategoriKET"]; ?></td>
                    <td><?= $data["kategoriREFERENCE"]; ?></td>
                    <td><a href="editwisata.php?kategoriKODE=<?= $data["kategoriKODE"]; ?>"><button type="submit" name="update" class="btn btn-secondary">Update</button></a>
                        <a href="hapuswisata.php?kategoriKODE=<?= $data["kategoriKODE"]; ?>"> <button type="submit" name="delete" class="btn btn-secondary">Delete</button></a>
                    </td>

                </tr>
                <?php $x++; ?>
            <?php endforeach; ?>

        </tbody>
    </table>


</section>


<?php

include '../_footer.php';
?>